package geo.iplocation.utils;

import java.io.IOException;
import java.net.InetAddress;

import geo.iplocation.model.IP2Location;
import geo.iplocation.model.IPResult;

public class IPToLocationUtils {

    private static final String CONFIG_PATH_IPV6 = "resources/IPLocal/" + "IPV6-COUNTRY-REGION-CITY.BIN";

    private static final String CONFIG_PATH_IPV4 = "resources/IPLocal/" + "IP-COUNTRY-REGION-CITY.BIN";

    private static final IP2Location locIpV6 = new IP2Location();
    private static final IP2Location locIpV4 = new IP2Location();

    static {
        locIpV6.IPDatabasePath = CONFIG_PATH_IPV6;
        locIpV4.IPDatabasePath = CONFIG_PATH_IPV4;
    }

    public static String getProvince(String ipAddress) {
        try {
            if (isIpV4(InetAddress.getByName(ipAddress))) {
                return locIpV4.IPQuery(ipAddress).getRegion().toString();
            } else if (isIpV6(InetAddress.getByName(ipAddress))) {
                return locIpV6.IPQuery(ipAddress).getRegion().toString();
            }
        } catch (IOException e) {
            // Log here
        }
        return "1";

    }

    private static boolean isIpV4(InetAddress ia) {
        return ia instanceof java.net.Inet4Address;
    }

    private static boolean isIpV6(InetAddress ia) {
        return ia instanceof java.net.Inet6Address;
    }

    public static void main(String[] args) {

        try {

            IPResult rec = locIpV6.IPQuery("2402:800:61c5:baf2:150a:e52c:18f0:50db");
            System.out.println(rec.getRegion().toString());

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace(System.out);
        }

    }
}
