package geo.iplocation.model;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IP2Location
{
    private static final Pattern pattern;
    private static final Pattern pattern2;
    private static final Pattern pattern3;
    private static final Pattern pattern4;
    private static final Pattern pattern5;
    private static final Pattern pattern6;
    private static final BigInteger MAX_IPV4_RANGE;
    private static final BigInteger MAX_IPV6_RANGE;
    private static final BigInteger FROM_6TO4;
    private static final BigInteger TO_6TO4;
    private static final BigInteger FROM_TEREDO;
    private static final BigInteger TO_TEREDO;
    private static final BigInteger LAST_32BITS;
    private static final int[] COUNTRY_POSITION;
    private static final int[] REGION_POSITION;
    private static final int[] CITY_POSITION;
    private static final int[] ISP_POSITION;
    private static final int[] LATITUDE_POSITION;
    private static final int[] LONGITUDE_POSITION;
    private static final int[] DOMAIN_POSITION;
    private static final int[] ZIPCODE_POSITION;
    private static final int[] TIMEZONE_POSITION;
    private static final int[] NETSPEED_POSITION;
    private static final int[] IDDCODE_POSITION;
    private static final int[] AREACODE_POSITION;
    private static final int[] WEATHERSTATIONCODE_POSITION;
    private static final int[] WEATHERSTATIONNAME_POSITION;
    private static final int[] MCC_POSITION;
    private static final int[] MNC_POSITION;
    private static final int[] MOBILEBRAND_POSITION;
    private static final int[] ELEVATION_POSITION;
    private static final int[] USAGETYPE_POSITION;
    private MetaData _MetaData;
    private MappedByteBuffer _IPv4Buffer;
    private MappedByteBuffer _IPv6Buffer;
    private MappedByteBuffer _MapDataBuffer;
    private int[][] _IndexArrayIPv4;
    private int[][] _IndexArrayIPv6;
    private long _IPv4Offset;
    private long _IPv6Offset;
    private long _MapDataOffset;
    private int _IPv4ColumnSize;
    private int _IPv6ColumnSize;
    public boolean UseMemoryMappedFile;
    public String IPDatabasePath;
    public String IPLicensePath;
    private boolean gotdelay;
    private boolean _alreadyCheckedKey;
    private int COUNTRY_POSITION_OFFSET;
    private int REGION_POSITION_OFFSET;
    private int CITY_POSITION_OFFSET;
    private int ISP_POSITION_OFFSET;
    private int DOMAIN_POSITION_OFFSET;
    private int ZIPCODE_POSITION_OFFSET;
    private int LATITUDE_POSITION_OFFSET;
    private int LONGITUDE_POSITION_OFFSET;
    private int TIMEZONE_POSITION_OFFSET;
    private int NETSPEED_POSITION_OFFSET;
    private int IDDCODE_POSITION_OFFSET;
    private int AREACODE_POSITION_OFFSET;
    private int WEATHERSTATIONCODE_POSITION_OFFSET;
    private int WEATHERSTATIONNAME_POSITION_OFFSET;
    private int MCC_POSITION_OFFSET;
    private int MNC_POSITION_OFFSET;
    private int MOBILEBRAND_POSITION_OFFSET;
    private int ELEVATION_POSITION_OFFSET;
    private int USAGETYPE_POSITION_OFFSET;
    private boolean COUNTRY_ENABLED;
    private boolean REGION_ENABLED;
    private boolean CITY_ENABLED;
    private boolean ISP_ENABLED;
    private boolean LATITUDE_ENABLED;
    private boolean LONGITUDE_ENABLED;
    private boolean DOMAIN_ENABLED;
    private boolean ZIPCODE_ENABLED;
    private boolean TIMEZONE_ENABLED;
    private boolean NETSPEED_ENABLED;
    private boolean IDDCODE_ENABLED;
    private boolean AREACODE_ENABLED;
    private boolean WEATHERSTATIONCODE_ENABLED;
    private boolean WEATHERSTATIONNAME_ENABLED;
    private boolean MCC_ENABLED;
    private boolean MNC_ENABLED;
    private boolean MOBILEBRAND_ENABLED;
    private boolean ELEVATION_ENABLED;
    private boolean USAGETYPE_ENABLED;
    
    public IP2Location() {
        this._MetaData = null;
        this._IPv4Buffer = null;
        this._IPv6Buffer = null;
        this._MapDataBuffer = null;
        this._IndexArrayIPv4 = new int[65536][2];
        this._IndexArrayIPv6 = new int[65536][2];
        this._IPv4Offset = 0L;
        this._IPv6Offset = 0L;
        this._MapDataOffset = 0L;
        this._IPv4ColumnSize = 0;
        this._IPv6ColumnSize = 0;
        this.UseMemoryMappedFile = false;
        this.IPDatabasePath = "";
        this.IPLicensePath = "";
        this.gotdelay = false;
        this._alreadyCheckedKey = false;
    }
    
    public void Close() {
        if (this._MetaData != null) {
            this._MetaData = null;
        }
        this.DestroyMappedBytes();
    }
    
    private void DestroyMappedBytes() {
        if (this._IPv4Buffer != null) {
            this._IPv4Buffer = null;
        }
        if (this._IPv6Buffer != null) {
            this._IPv6Buffer = null;
        }
        if (this._MapDataBuffer != null) {
            this._MapDataBuffer = null;
        }
    }
    
    private void CreateMappedBytes() throws IOException {
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(this.IPDatabasePath, "r");
            this.CreateMappedBytes(randomAccessFile.getChannel());
        }
        catch (IOException ex) {
            throw ex;
        }
        finally {
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
        }
    }
    
    private void CreateMappedBytes(final FileChannel fileChannel) throws IOException {
        try {
            if (this._IPv4Buffer == null) {
                final long n = this._IPv4ColumnSize * (long)this._MetaData.getDBCount();
                this._IPv4Offset = this._MetaData.getBaseAddr() - 1;
                (this._IPv4Buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, this._IPv4Offset, n)).order(ByteOrder.LITTLE_ENDIAN);
                this._MapDataOffset = this._IPv4Offset + n;
            }
            if (!this._MetaData.getOldBIN() && this._IPv6Buffer == null) {
                final long n2 = this._IPv6ColumnSize * (long)this._MetaData.getDBCountIPv6();
                this._IPv6Offset = this._MetaData.getBaseAddrIPv6() - 1;
                (this._IPv6Buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, this._IPv6Offset, n2)).order(ByteOrder.LITTLE_ENDIAN);
                this._MapDataOffset = this._IPv6Offset + n2;
            }
            if (this._MapDataBuffer == null) {
                (this._MapDataBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, this._MapDataOffset, fileChannel.size() - this._MapDataOffset)).order(ByteOrder.LITTLE_ENDIAN);
            }
        }
        catch (IOException ex) {
            throw ex;
        }
    }
    
    private boolean LoadBIN() throws IOException {
        boolean b = false;
        RandomAccessFile randomAccessFile = null;
        try {
            if (this.IPDatabasePath.length() > 0) {
                randomAccessFile = new RandomAccessFile(this.IPDatabasePath, "r");
                final FileChannel channel = randomAccessFile.getChannel();
                final MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_ONLY, 0L, 64L);
                map.order(ByteOrder.LITTLE_ENDIAN);
                (this._MetaData = new MetaData()).setDBType(map.get(0));
                this._MetaData.setDBColumn(map.get(1));
                this._MetaData.setDBYear(map.get(2));
                this._MetaData.setDBMonth(map.get(3));
                this._MetaData.setDBDay(map.get(4));
                this._MetaData.setDBCount(map.getInt(5));
                this._MetaData.setBaseAddr(map.getInt(9));
                this._MetaData.setDBCountIPv6(map.getInt(13));
                this._MetaData.setBaseAddrIPv6(map.getInt(17));
                this._MetaData.setIndexBaseAddr(map.getInt(21));
                this._MetaData.setIndexBaseAddrIPv6(map.getInt(25));
                if (this._MetaData.getIndexBaseAddr() > 0) {
                    this._MetaData.setIndexed(true);
                }
                if (this._MetaData.getDBCountIPv6() == 0) {
                    this._MetaData.setOldBIN(true);
                }
                else if (this._MetaData.getIndexBaseAddrIPv6() > 0) {
                    this._MetaData.setIndexedIPv6(true);
                }
                final int dbColumn = this._MetaData.getDBColumn();
                this._IPv4ColumnSize = dbColumn << 2;
                this._IPv6ColumnSize = 16 + (dbColumn - 1 << 2);
                final int dbType = this._MetaData.getDBType();
                this.COUNTRY_POSITION_OFFSET = ((IP2Location.COUNTRY_POSITION[dbType] != 0) ? (IP2Location.COUNTRY_POSITION[dbType] - 2 << 2) : 0);
                this.REGION_POSITION_OFFSET = ((IP2Location.REGION_POSITION[dbType] != 0) ? (IP2Location.REGION_POSITION[dbType] - 2 << 2) : 0);
                this.CITY_POSITION_OFFSET = ((IP2Location.CITY_POSITION[dbType] != 0) ? (IP2Location.CITY_POSITION[dbType] - 2 << 2) : 0);
                this.ISP_POSITION_OFFSET = ((IP2Location.ISP_POSITION[dbType] != 0) ? (IP2Location.ISP_POSITION[dbType] - 2 << 2) : 0);
                this.DOMAIN_POSITION_OFFSET = ((IP2Location.DOMAIN_POSITION[dbType] != 0) ? (IP2Location.DOMAIN_POSITION[dbType] - 2 << 2) : 0);
                this.ZIPCODE_POSITION_OFFSET = ((IP2Location.ZIPCODE_POSITION[dbType] != 0) ? (IP2Location.ZIPCODE_POSITION[dbType] - 2 << 2) : 0);
                this.LATITUDE_POSITION_OFFSET = ((IP2Location.LATITUDE_POSITION[dbType] != 0) ? (IP2Location.LATITUDE_POSITION[dbType] - 2 << 2) : 0);
                this.LONGITUDE_POSITION_OFFSET = ((IP2Location.LONGITUDE_POSITION[dbType] != 0) ? (IP2Location.LONGITUDE_POSITION[dbType] - 2 << 2) : 0);
                this.TIMEZONE_POSITION_OFFSET = ((IP2Location.TIMEZONE_POSITION[dbType] != 0) ? (IP2Location.TIMEZONE_POSITION[dbType] - 2 << 2) : 0);
                this.NETSPEED_POSITION_OFFSET = ((IP2Location.NETSPEED_POSITION[dbType] != 0) ? (IP2Location.NETSPEED_POSITION[dbType] - 2 << 2) : 0);
                this.IDDCODE_POSITION_OFFSET = ((IP2Location.IDDCODE_POSITION[dbType] != 0) ? (IP2Location.IDDCODE_POSITION[dbType] - 2 << 2) : 0);
                this.AREACODE_POSITION_OFFSET = ((IP2Location.AREACODE_POSITION[dbType] != 0) ? (IP2Location.AREACODE_POSITION[dbType] - 2 << 2) : 0);
                this.WEATHERSTATIONCODE_POSITION_OFFSET = ((IP2Location.WEATHERSTATIONCODE_POSITION[dbType] != 0) ? (IP2Location.WEATHERSTATIONCODE_POSITION[dbType] - 2 << 2) : 0);
                this.WEATHERSTATIONNAME_POSITION_OFFSET = ((IP2Location.WEATHERSTATIONNAME_POSITION[dbType] != 0) ? (IP2Location.WEATHERSTATIONNAME_POSITION[dbType] - 2 << 2) : 0);
                this.MCC_POSITION_OFFSET = ((IP2Location.MCC_POSITION[dbType] != 0) ? (IP2Location.MCC_POSITION[dbType] - 2 << 2) : 0);
                this.MNC_POSITION_OFFSET = ((IP2Location.MNC_POSITION[dbType] != 0) ? (IP2Location.MNC_POSITION[dbType] - 2 << 2) : 0);
                this.MOBILEBRAND_POSITION_OFFSET = ((IP2Location.MOBILEBRAND_POSITION[dbType] != 0) ? (IP2Location.MOBILEBRAND_POSITION[dbType] - 2 << 2) : 0);
                this.ELEVATION_POSITION_OFFSET = ((IP2Location.ELEVATION_POSITION[dbType] != 0) ? (IP2Location.ELEVATION_POSITION[dbType] - 2 << 2) : 0);
                this.USAGETYPE_POSITION_OFFSET = ((IP2Location.USAGETYPE_POSITION[dbType] != 0) ? (IP2Location.USAGETYPE_POSITION[dbType] - 2 << 2) : 0);
                this.COUNTRY_ENABLED = (IP2Location.COUNTRY_POSITION[dbType] != 0);
                this.REGION_ENABLED = (IP2Location.REGION_POSITION[dbType] != 0);
                this.CITY_ENABLED = (IP2Location.CITY_POSITION[dbType] != 0);
                this.ISP_ENABLED = (IP2Location.ISP_POSITION[dbType] != 0);
                this.LATITUDE_ENABLED = (IP2Location.LATITUDE_POSITION[dbType] != 0);
                this.LONGITUDE_ENABLED = (IP2Location.LONGITUDE_POSITION[dbType] != 0);
                this.DOMAIN_ENABLED = (IP2Location.DOMAIN_POSITION[dbType] != 0);
                this.ZIPCODE_ENABLED = (IP2Location.ZIPCODE_POSITION[dbType] != 0);
                this.TIMEZONE_ENABLED = (IP2Location.TIMEZONE_POSITION[dbType] != 0);
                this.NETSPEED_ENABLED = (IP2Location.NETSPEED_POSITION[dbType] != 0);
                this.IDDCODE_ENABLED = (IP2Location.IDDCODE_POSITION[dbType] != 0);
                this.AREACODE_ENABLED = (IP2Location.AREACODE_POSITION[dbType] != 0);
                this.WEATHERSTATIONCODE_ENABLED = (IP2Location.WEATHERSTATIONCODE_POSITION[dbType] != 0);
                this.WEATHERSTATIONNAME_ENABLED = (IP2Location.WEATHERSTATIONNAME_POSITION[dbType] != 0);
                this.MCC_ENABLED = (IP2Location.MCC_POSITION[dbType] != 0);
                this.MNC_ENABLED = (IP2Location.MNC_POSITION[dbType] != 0);
                this.MOBILEBRAND_ENABLED = (IP2Location.MOBILEBRAND_POSITION[dbType] != 0);
                this.ELEVATION_ENABLED = (IP2Location.ELEVATION_POSITION[dbType] != 0);
                this.USAGETYPE_ENABLED = (IP2Location.USAGETYPE_POSITION[dbType] != 0);
                if (this._MetaData.getIndexed()) {
                    final MappedByteBuffer map2 = channel.map(FileChannel.MapMode.READ_ONLY, this._MetaData.getIndexBaseAddr() - 1, this._MetaData.getBaseAddr() - this._MetaData.getIndexBaseAddr());
                    map2.order(ByteOrder.LITTLE_ENDIAN);
                    int n = 0;
                    for (int i = 0; i < this._IndexArrayIPv4.length; ++i) {
                        this._IndexArrayIPv4[i][0] = map2.getInt(n);
                        this._IndexArrayIPv4[i][1] = map2.getInt(n + 4);
                        n += 8;
                    }
                    if (this._MetaData.getIndexedIPv6()) {
                        for (int j = 0; j < this._IndexArrayIPv6.length; ++j) {
                            this._IndexArrayIPv6[j][0] = map2.getInt(n);
                            this._IndexArrayIPv6[j][1] = map2.getInt(n + 4);
                            n += 8;
                        }
                    }
                }
                if (this.UseMemoryMappedFile) {
                    this.CreateMappedBytes(channel);
                }
                else {
                    this.DestroyMappedBytes();
                }
                b = true;
            }
        }
        catch (IOException ex) {
            throw ex;
        }
        finally {
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
        }
        return b;
    }
    
    @Override
    @Deprecated
    protected void finalize() throws Throwable {
        super.finalize();
    }
    
    public IPResult IPQuery(String trim) throws IOException {
        trim = trim.trim();
        final IPResult ipResult = new IPResult(trim);
        RandomAccessFile randomAccessFile = null;
        ByteBuffer byteBuffer = null;
        ByteBuffer duplicate = null;
        try {
            if (trim == null || trim.length() == 0) {
                ipResult.status = "EMPTY_IP_ADDRESS";
                return ipResult;
            }
            int n = 0;
            int n2 = 0;
            final BigInteger zero = BigInteger.ZERO;
            boolean b = false;
            int n3;
            BigInteger subtract;
            try {
                final BigInteger[] ip2no = this.ip2no(trim);
                n3 = ip2no[0].intValue();
                subtract = ip2no[1];
                if (ip2no[2].intValue() == 6) {
                    final String[] expandIPv6 = this.ExpandIPv6(trim, n3);
                    ipResult.ip_address = expandIPv6[0];
                    n3 = Integer.parseInt(expandIPv6[1]);
                }
            }
            catch (UnknownHostException ex2) {
                ipResult.status = "INVALID_IP_ADDRESS";
                return ipResult;
            }
            //this.checkLicense();
            //ipResult.delay = this.delay();
            long n4 = 0L;
            final BigInteger zero2 = BigInteger.ZERO;
            final BigInteger zero3 = BigInteger.ZERO;
            if (this._MetaData == null && !this.LoadBIN()) {
                ipResult.status = "MISSING_FILE";
                return ipResult;
            }
            if (this.UseMemoryMappedFile) {
                if (this._IPv4Buffer == null || (!this._MetaData.getOldBIN() && this._IPv6Buffer == null) || this._MapDataBuffer == null) {
                    this.CreateMappedBytes();
                }
            }
            else {
                this.DestroyMappedBytes();
                randomAccessFile = new RandomAccessFile(this.IPDatabasePath, "r");
                if (randomAccessFile == null) {
                    ipResult.status = "MISSING_FILE";
                    return ipResult;
                }
            }
            BigInteger val;
            long n5;
            int n6;
            if (n3 == 4) {
                val = IP2Location.MAX_IPV4_RANGE;
                n5 = this._MetaData.getDBCount();
                if (this.UseMemoryMappedFile) {
                    byteBuffer = this._IPv4Buffer.duplicate();
                    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                    n2 = byteBuffer.capacity();
                }
                else {
                    n = this._MetaData.getBaseAddr();
                }
                n6 = this._IPv4ColumnSize;
                if (this._MetaData.getIndexed()) {
                    final int intValue = subtract.shiftRight(16).intValue();
                    n4 = this._IndexArrayIPv4[intValue][0];
                    n5 = this._IndexArrayIPv4[intValue][1];
                }
            }
            else {
                if (this._MetaData.getOldBIN()) {
                    ipResult.status = "IPV6_NOT_SUPPORTED";
                    return ipResult;
                }
                val = IP2Location.MAX_IPV6_RANGE;
                n5 = this._MetaData.getDBCountIPv6();
                if (this.UseMemoryMappedFile) {
                    byteBuffer = this._IPv6Buffer.duplicate();
                    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
                    n2 = byteBuffer.capacity();
                }
                else {
                    n = this._MetaData.getBaseAddrIPv6();
                }
                n6 = this._IPv6ColumnSize;
                if (this._MetaData.getIndexedIPv6()) {
                    final int intValue2 = subtract.shiftRight(112).intValue();
                    n4 = this._IndexArrayIPv6[intValue2][0];
                    n5 = this._IndexArrayIPv6[intValue2][1];
                }
            }
            if (subtract.compareTo(val) == 0) {
                subtract = subtract.subtract(BigInteger.ONE);
            }
            while (n4 <= n5) {
                final long n7 = (n4 + n5) / 2L;
                final long n8 = n + n7 * n6;
                final long n9 = n8 + n6;
                if (this.UseMemoryMappedFile) {
                    b = (n9 >= n2);
                }
                final BigInteger read32or128 = this.read32or128(n8, n3, byteBuffer, randomAccessFile);
                final BigInteger val2 = b ? BigInteger.ZERO : this.read32or128(n9, n3, byteBuffer, randomAccessFile);
                if (subtract.compareTo(read32or128) >= 0 && subtract.compareTo(val2) < 0) {
                    int n10 = 4;
                    if (n3 == 6) {
                        n10 = 16;
                    }
                    final byte[] readrow = this.readrow(n8 + n10, n6 - n10, byteBuffer, randomAccessFile);
                    if (this.UseMemoryMappedFile) {
                        duplicate = this._MapDataBuffer.duplicate();
                        duplicate.order(ByteOrder.LITTLE_ENDIAN);
                    }
                    if (this.COUNTRY_ENABLED) {
                        final long longValue = this.read32_row(readrow, this.COUNTRY_POSITION_OFFSET).longValue();
                        ipResult.country_short = this.readStr(longValue, duplicate, randomAccessFile);
                        ipResult.country_long = this.readStr(longValue + 3L, duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.country_short = "Not_Supported";
                        ipResult.country_long = "Not_Supported";
                    }
                    if (this.REGION_ENABLED) {
                        ipResult.region = this.readStr(this.read32_row(readrow, this.REGION_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.region = "Not_Supported";
                    }
                    if (this.CITY_ENABLED) {
                        ipResult.city = this.readStr(this.read32_row(readrow, this.CITY_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.city = "Not_Supported";
                    }
                    if (this.ISP_ENABLED) {
                        ipResult.isp = this.readStr(this.read32_row(readrow, this.ISP_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.isp = "Not_Supported";
                    }
                    if (this.LATITUDE_ENABLED) {
                        ipResult.latitude = Float.parseFloat(this.setDecimalPlaces(this.readFloat_row(readrow, this.LATITUDE_POSITION_OFFSET)));
                    }
                    else {
                        ipResult.latitude = 0.0f;
                    }
                    if (this.LONGITUDE_ENABLED) {
                        ipResult.longitude = Float.parseFloat(this.setDecimalPlaces(this.readFloat_row(readrow, this.LONGITUDE_POSITION_OFFSET)));
                    }
                    else {
                        ipResult.longitude = 0.0f;
                    }
                    if (this.DOMAIN_ENABLED) {
                        ipResult.domain = this.readStr(this.read32_row(readrow, this.DOMAIN_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.domain = "Not_Supported";
                    }
                    if (this.ZIPCODE_ENABLED) {
                        ipResult.zipcode = this.readStr(this.read32_row(readrow, this.ZIPCODE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.zipcode = "Not_Supported";
                    }
                    if (this.TIMEZONE_ENABLED) {
                        ipResult.timezone = this.readStr(this.read32_row(readrow, this.TIMEZONE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.timezone = "Not_Supported";
                    }
                    if (this.NETSPEED_ENABLED) {
                        ipResult.netspeed = this.readStr(this.read32_row(readrow, this.NETSPEED_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.netspeed = "Not_Supported";
                    }
                    if (this.IDDCODE_ENABLED) {
                        ipResult.iddcode = this.readStr(this.read32_row(readrow, this.IDDCODE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.iddcode = "Not_Supported";
                    }
                    if (this.AREACODE_ENABLED) {
                        ipResult.areacode = this.readStr(this.read32_row(readrow, this.AREACODE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.areacode = "Not_Supported";
                    }
                    if (this.WEATHERSTATIONCODE_ENABLED) {
                        ipResult.weatherstationcode = this.readStr(this.read32_row(readrow, this.WEATHERSTATIONCODE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.weatherstationcode = "Not_Supported";
                    }
                    if (this.WEATHERSTATIONNAME_ENABLED) {
                        ipResult.weatherstationname = this.readStr(this.read32_row(readrow, this.WEATHERSTATIONNAME_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.weatherstationname = "Not_Supported";
                    }
                    if (this.MCC_ENABLED) {
                        ipResult.mcc = this.readStr(this.read32_row(readrow, this.MCC_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.mcc = "Not_Supported";
                    }
                    if (this.MNC_ENABLED) {
                        ipResult.mnc = this.readStr(this.read32_row(readrow, this.MNC_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.mnc = "Not_Supported";
                    }
                    if (this.MOBILEBRAND_ENABLED) {
                        ipResult.mobilebrand = this.readStr(this.read32_row(readrow, this.MOBILEBRAND_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.mobilebrand = "Not_Supported";
                    }
                    if (this.ELEVATION_ENABLED) {
                        ipResult.elevation = this.convertFloat(this.readStr(this.read32_row(readrow, this.ELEVATION_POSITION_OFFSET).longValue(), duplicate, randomAccessFile));
                    }
                    else {
                        ipResult.elevation = 0.0f;
                    }
                    if (this.USAGETYPE_ENABLED) {
                        ipResult.usagetype = this.readStr(this.read32_row(readrow, this.USAGETYPE_POSITION_OFFSET).longValue(), duplicate, randomAccessFile);
                    }
                    else {
                        ipResult.usagetype = "Not_Supported";
                    }
                    ipResult.status = "OK";
                    break;
                }
                if (subtract.compareTo(read32or128) < 0) {
                    n5 = n7 - 1L;
                }
                else {
                    n4 = n7 + 1L;
                }
            }
            return ipResult;
        }
        catch (IOException ex) {
            throw ex;
        }
        finally {
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
        }
    }
    
    private String[] ExpandIPv6(final String s, final int i) {
        String input = s.toUpperCase();
        String value = String.valueOf(i);
        if (i == 4) {
            if (IP2Location.pattern4.matcher(input).matches()) {
                input = input.replaceAll("::", "0000:0000:0000:0000:0000:");
            }
            else {
                final Matcher matcher = IP2Location.pattern5.matcher(input);
                if (matcher.matches()) {
                    final String group = matcher.group(1);
                    final String[] split = group.replaceAll("^:+", "").replaceAll(":+$", "").split(":");
                    final int length = split.length;
                    final StringBuffer sb = new StringBuffer(32);
                    for (final String str : split) {
                        sb.append("0000".substring(str.length()) + str);
                    }
                    long longValue = new BigInteger(sb.toString(), 16).longValue();
                    final long[] array = { 0L, 0L, 0L, 0L };
                    for (int k = 0; k < 4; ++k) {
                        array[k] = (longValue & 0xFFL);
                        longValue >>= 8;
                    }
                    input = input.replaceAll(group + "$", ":" + array[3] + "." + array[2] + "." + array[1] + "." + array[0]).replaceAll("::", "0000:0000:0000:0000:0000:");
                }
            }
        }
        else if (i == 6) {
            if (input.equals("::")) {
                input = (input + "0.0.0.0").replaceAll("::", "0000:0000:0000:0000:0000:FFFF:");
                value = "4";
            }
            else {
                final Matcher matcher2 = IP2Location.pattern4.matcher(input);
                if (matcher2.matches()) {
                    final String group2 = matcher2.group(1);
                    final String group3 = matcher2.group(2);
                    final String[] split2 = group3.split("\\.");
                    final int[] array2 = new int[4];
                    for (int length2 = array2.length, l = 0; l < length2; ++l) {
                        array2[l] = Integer.parseInt(split2[l]);
                    }
                    final int m = (array2[0] << 8) + array2[1];
                    final int i2 = (array2[2] << 8) + array2[3];
                    final String hexString = Integer.toHexString(m);
                    final String hexString2 = Integer.toHexString(i2);
                    final StringBuffer sb2 = new StringBuffer(group2.length() + 9);
                    sb2.append(group2);
                    sb2.append("0000".substring(hexString.length()));
                    sb2.append(hexString);
                    sb2.append(":");
                    sb2.append("0000".substring(hexString2.length()));
                    sb2.append(hexString2);
                    final String[] split3 = sb2.toString().toUpperCase().split("::");
                    final String[] split4 = split3[0].split(":");
                    final StringBuffer sb3 = new StringBuffer(40);
                    final StringBuffer sb4 = new StringBuffer(40);
                    final StringBuffer sb5 = new StringBuffer(40);
                    final int length3 = split4.length;
                    int n = 0;
                    for (int n2 = 0; n2 < length3; ++n2) {
                        if (split4[n2].length() > 0) {
                            ++n;
                            sb3.append("0000".substring(split4[n2].length()));
                            sb3.append(split4[n2]);
                            sb3.append(":");
                        }
                    }
                    if (split3.length > 1) {
                        final String[] split5 = split3[1].split(":");
                        for (int length4 = split5.length, n3 = 0; n3 < length4; ++n3) {
                            if (split5[n3].length() > 0) {
                                ++n;
                                sb4.append("0000".substring(split5[n3].length()));
                                sb4.append(split5[n3]);
                                sb4.append(":");
                            }
                        }
                    }
                    final int n4 = 8 - n;
                    if (n4 == 6) {
                        for (int n5 = 1; n5 < n4; ++n5) {
                            sb5.append("0000");
                            sb5.append(":");
                        }
                        sb5.append("FFFF:");
                        sb5.append(group3);
                        value = "4";
                        input = sb5.toString();
                    }
                    else {
                        for (int n6 = 0; n6 < n4; ++n6) {
                            sb5.append("0000");
                            sb5.append(":");
                        }
                        sb3.append(sb5).append(sb4);
                        input = sb3.toString().replaceAll(":$", "");
                    }
                }
                else {
                    final Matcher matcher3 = IP2Location.pattern6.matcher(input);
                    if (matcher3.matches()) {
                        final String group4 = matcher3.group(1);
                        final String[] split6 = group4.replaceAll("^:+", "").replaceAll(":+$", "").split(":");
                        final int length5 = split6.length;
                        final StringBuffer sb6 = new StringBuffer(32);
                        for (final String str2 : split6) {
                            sb6.append("0000".substring(str2.length()) + str2);
                        }
                        long longValue2 = new BigInteger(sb6.toString(), 16).longValue();
                        final long[] array3 = { 0L, 0L, 0L, 0L };
                        for (int n8 = 0; n8 < 4; ++n8) {
                            array3[n8] = (longValue2 & 0xFFL);
                            longValue2 >>= 8;
                        }
                        input = input.replaceAll(group4 + "$", ":" + array3[3] + "." + array3[2] + "." + array3[1] + "." + array3[0]).replaceAll("::", "0000:0000:0000:0000:0000:FFFF:");
                        value = "4";
                    }
                    else {
                        final String[] split7 = input.split("::");
                        final String[] split8 = split7[0].split(":");
                        final StringBuffer sb7 = new StringBuffer(40);
                        final StringBuffer sb8 = new StringBuffer(40);
                        final StringBuffer sb9 = new StringBuffer(40);
                        final int length6 = split8.length;
                        int n9 = 0;
                        for (int n10 = 0; n10 < length6; ++n10) {
                            if (split8[n10].length() > 0) {
                                ++n9;
                                sb7.append("0000".substring(split8[n10].length()));
                                sb7.append(split8[n10]);
                                sb7.append(":");
                            }
                        }
                        if (split7.length > 1) {
                            final String[] split9 = split7[1].split(":");
                            for (int length7 = split9.length, n11 = 0; n11 < length7; ++n11) {
                                if (split9[n11].length() > 0) {
                                    ++n9;
                                    sb8.append("0000".substring(split9[n11].length()));
                                    sb8.append(split9[n11]);
                                    sb8.append(":");
                                }
                            }
                        }
                        for (int n12 = 8 - n9, n13 = 0; n13 < n12; ++n13) {
                            sb9.append("0000");
                            sb9.append(":");
                        }
                        sb7.append(sb9).append(sb8);
                        input = sb7.toString().replaceAll(":$", "");
                    }
                }
            }
        }
        return new String[] { input, value };
    }
    
    private float convertFloat(final String s) {
        try {
            return Float.parseFloat(s);
        }
        catch (NumberFormatException ex) {
            return 0.0f;
        }
    }
    
    private void reverse(final byte[] array) {
        if (array == null) {
            return;
        }
        for (int n = 0, i = array.length - 1; i > n; --i, ++n) {
            final byte b = array[i];
            array[i] = array[n];
            array[n] = b;
        }
    }
    
    private byte[] readrow(final long n, final long n2, final ByteBuffer byteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        final byte[] array = new byte[(int)n2];
        if (this.UseMemoryMappedFile) {
            byteBuffer.position((int)n);
            byteBuffer.get(array, 0, (int)n2);
        }
        else {
            randomAccessFile.seek(n - 1L);
            randomAccessFile.read(array, 0, (int)n2);
        }
        return array;
    }
    
    private BigInteger read32or128(final long n, final int n2, final ByteBuffer byteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        if (n2 == 4) {
            return this.read32(n, byteBuffer, randomAccessFile);
        }
        if (n2 == 6) {
            return this.read128(n, byteBuffer, randomAccessFile);
        }
        return BigInteger.ZERO;
    }
    
    private BigInteger read128(final long n, final ByteBuffer byteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        final BigInteger zero = BigInteger.ZERO;
        final byte[] magnitude = new byte[16];
        if (this.UseMemoryMappedFile) {
            byteBuffer.position((int)n);
            byteBuffer.get(magnitude, 0, 16);
        }
        else {
            randomAccessFile.seek(n - 1L);
            randomAccessFile.read(magnitude, 0, 16);
        }
        this.reverse(magnitude);
        return new BigInteger(1, magnitude);
    }
    
    private BigInteger read32_row(final byte[] array, final int n) throws IOException {
        final byte[] magnitude = new byte[4];
        System.arraycopy(array, n, magnitude, 0, 4);
        this.reverse(magnitude);
        return new BigInteger(1, magnitude);
    }
    
    private BigInteger read32(final long n, final ByteBuffer byteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        if (this.UseMemoryMappedFile) {
            return BigInteger.valueOf((long)byteBuffer.getInt((int)n) & 0xFFFFFFFFL);
        }
        randomAccessFile.seek(n - 1L);
        final byte[] array = new byte[4];
        randomAccessFile.read(array, 0, 4);
        this.reverse(array);
        return new BigInteger(1, array);
    }
    
    private String readStr(long pos, final ByteBuffer byteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        if (this.UseMemoryMappedFile) {
            pos -= this._MapDataOffset;
            final byte value = this._MapDataBuffer.get((int)pos);
            try {
                final byte[] array = new byte[value];
                byteBuffer.position((int)pos + 1);
                byteBuffer.get(array, 0, value);
                return new String(array);
            }
            catch (NegativeArraySizeException ex) {
                return null;
            }
        }
        randomAccessFile.seek(pos);
        final int read = randomAccessFile.read();
        byte[] array;
        try {
            array = new byte[read];
            randomAccessFile.read(array, 0, read);
        }
        catch (NegativeArraySizeException ex2) {
            return null;
        }
        return new String(array);
    }
    
    private float readFloat_row(final byte[] array, final int n) {
        final byte[] array2 = new byte[4];
        System.arraycopy(array, n, array2, 0, 4);
        return Float.intBitsToFloat((array2[3] & 0xFF) << 24 | (array2[2] & 0xFF) << 16 | (array2[1] & 0xFF) << 8 | (array2[0] & 0xFF));
    }
    
    private float readFloat(final long n, final MappedByteBuffer mappedByteBuffer, final RandomAccessFile randomAccessFile) throws IOException {
        if (this.UseMemoryMappedFile) {
            return mappedByteBuffer.getFloat((int)n);
        }
        randomAccessFile.seek(n - 1L);
        final int[] array = new int[4];
        for (int i = 0; i < 4; ++i) {
            array[i] = randomAccessFile.read();
        }
        return Float.intBitsToFloat(array[3] << 24 | array[2] << 16 | array[1] << 8 | array[0]);
    }
    
    private String setDecimalPlaces(final float n) {
        final DecimalFormat decimalFormat = (DecimalFormat)NumberFormat.getNumberInstance(Locale.getDefault());
        decimalFormat.applyPattern("###.######");
        return decimalFormat.format(n).replace(',', '.');
    }
    
    private BigInteger[] ip2no(final String s) throws UnknownHostException {
        final BigInteger zero = BigInteger.ZERO;
        final BigInteger zero2 = BigInteger.ZERO;
        BigInteger bigInteger = new BigInteger("4");
        BigInteger bigInteger2;
        BigInteger bigInteger3;
        if (IP2Location.pattern.matcher(s).matches()) {
            bigInteger2 = new BigInteger("4");
            bigInteger3 = new BigInteger(String.valueOf(this.ipv4no(s)));
        }
        else {
            if (IP2Location.pattern2.matcher(s).matches() || IP2Location.pattern3.matcher(s).matches()) {
                throw new UnknownHostException();
            }
            bigInteger = new BigInteger("6");
            final InetAddress byName = InetAddress.getByName(s);
            final byte[] address = byName.getAddress();
            String val = "0";
            if (byName instanceof Inet6Address) {
                val = "6";
            }
            else if (byName instanceof Inet4Address) {
                val = "4";
            }
            bigInteger3 = new BigInteger(1, address);
            if (bigInteger3.compareTo(IP2Location.FROM_6TO4) >= 0 && bigInteger3.compareTo(IP2Location.TO_6TO4) <= 0) {
                val = "4";
                bigInteger3 = bigInteger3.shiftRight(80).and(IP2Location.LAST_32BITS);
                bigInteger = new BigInteger("4");
            }
            else if (bigInteger3.compareTo(IP2Location.FROM_TEREDO) >= 0 && bigInteger3.compareTo(IP2Location.TO_TEREDO) <= 0) {
                val = "4";
                bigInteger3 = bigInteger3.not().and(IP2Location.LAST_32BITS);
                bigInteger = new BigInteger("4");
            }
            bigInteger2 = new BigInteger(val);
        }
        return new BigInteger[] { bigInteger2, bigInteger3, bigInteger };
    }
    
    private long ipv4no(final String s) {
        final String[] split = s.split("\\.");
        long n = 0L;
        for (int i = 3; i >= 0; --i) {
            n |= Long.parseLong(split[3 - i]) << (i << 3);
        }
        return n;
    }
    
    private void checkLicense() {
        if (!this._alreadyCheckedKey) {
            if (this.IPLicensePath == null || this.IPLicensePath.length() == 0) {
                this.IPLicensePath = "license.key";
            }
            String line = "";
            String line2 = "";
            try {
                final BufferedReader bufferedReader = new BufferedReader(new FileReader(this.IPLicensePath));
                if (!bufferedReader.ready()) {
                    bufferedReader.close();
                    throw new IOException();
                }
                final String line3;
                if ((line3 = bufferedReader.readLine()) != null && (line = bufferedReader.readLine()) != null) {
                    line2 = bufferedReader.readLine();
                }
                bufferedReader.close();
                if (line2 == null || line2.length() == 0) {
                    if (!line.trim().equals(this.generateKey(line3))) {
                        this.gotdelay = true;
                    }
                }
                else if (!line2.trim().equals(this.generateKey(line3 + line))) {
                    this.gotdelay = true;
                }
                this._alreadyCheckedKey = true;
            }
            catch (IOException ex) {
                this.gotdelay = true;
            }
        }
    }
    
    private boolean delay() {
        boolean b = false;
        if (this.gotdelay) {
            final int n = 1 + (int)(Math.random() * 10.0);
            try {
                if (n == 10) {
                    b = true;
                    Thread.sleep(5000L);
                }
            }
            catch (Exception x) {
                System.out.println(x);
            }
        }
        return b;
    }
    
    private String generateKey(final String s) {
        final String[] array = new String[2];
        final String[] array2 = new String[62];
        final String[] array3 = new String[200];
        final StringBuffer sb = new StringBuffer(50);
        try {
            if (s.length() > 20) {
                array[0] = s.substring(1, 20);
            }
            else {
                array[0] = s;
            }
            array[1] = "Hexasoft";
            int n = 0;
            for (int i = 48; i <= 57; ++i) {
                array2[n] = String.valueOf((char)i);
                ++n;
            }
            for (int j = 65; j <= 90; ++j) {
                array2[n] = String.valueOf((char)j);
                ++n;
            }
            for (int k = 97; k <= 122; ++k) {
                array2[n] = String.valueOf((char)k);
                ++n;
            }
            final String[] array4 = { String.valueOf(this.Asc("7")), String.valueOf(this.Asc("0")), String.valueOf(this.Asc("a")), String.valueOf(this.Asc("1")), String.valueOf(this.Asc("b")), String.valueOf(this.Asc("2")), String.valueOf(this.Asc("c")), String.valueOf(this.Asc("3")), String.valueOf(this.Asc("d")), String.valueOf(this.Asc("4")), String.valueOf(this.Asc("e")), String.valueOf(this.Asc("5")), String.valueOf(this.Asc("f")), String.valueOf(this.Asc("6")), String.valueOf(this.Asc("g")) };
            for (int l = 0; l < 200; ++l) {
                array3[l] = array4[l % 15];
            }
            int n2 = 0;
            for (int n3 = 0; n3 <= 1; ++n3) {
                for (int n4 = 0; n4 <= 11; ++n4) {
                    for (int length = array[n3].length(), beginIndex = 0; beginIndex < length; ++beginIndex) {
                        final StringBuilder sb2 = new StringBuilder();
                        final String[] array5 = array3;
                        final int n5 = n2 % 200;
                        array5[n5] = sb2.append(array5[n5]).append(String.valueOf(this.Asc(array[n3].substring(beginIndex, beginIndex + 1)))).toString();
                        ++n2;
                    }
                    array3[n2 % 200] = String.valueOf(Long.parseLong(array3[n2 % 200]) >> 1);
                    ++n2;
                }
            }
            for (int n6 = 0; n6 <= 15; ++n6) {
                sb.append(array2[Integer.parseInt(array3[n6]) % 62]);
            }
        }
        catch (Exception x) {
            System.out.println(x);
        }
        if (sb.length() == 0) {
            return "error";
        }
        for (int n7 = 0; n7 < 3; ++n7) {
            sb.insert((n7 + 1 << 2) + n7, '-');
        }
        return sb.toString().toUpperCase();
    }
    
    private int Asc(final String str) {
        try {
            final String str2 = " !\"#$%&'()*+'-./0123456789:;<=>?@";
            final String str3 = "abcdefghijklmnopqrstuvwxyz";
            final int index = (str2 + str3.toUpperCase() + "[\\]^_`" + str3 + "{|}~").indexOf(str);
            if (index > -1) {
                return index + 32;
            }
        }
        catch (Exception x) {
            System.out.println(x);
        }
        return 0;
    }
    
    static {
        pattern = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        pattern2 = Pattern.compile("^([0-9A-F]{1,4}:){6}(0[0-9]+\\.|.*?\\.0[0-9]+).*$", 2);
        pattern3 = Pattern.compile("^[0-9]+$");
        pattern4 = Pattern.compile("^(.*:)(([0-9]+\\.){3}[0-9]+)$");
        pattern5 = Pattern.compile("^.*((:[0-9A-F]{1,4}){2})$");
        pattern6 = Pattern.compile("^[0:]+((:[0-9A-F]{1,4}){1,2})$", 2);
        MAX_IPV4_RANGE = new BigInteger("4294967295");
        MAX_IPV6_RANGE = new BigInteger("340282366920938463463374607431768211455");
        FROM_6TO4 = new BigInteger("42545680458834377588178886921629466624");
        TO_6TO4 = new BigInteger("42550872755692912415807417417958686719");
        FROM_TEREDO = new BigInteger("42540488161975842760550356425300246528");
        TO_TEREDO = new BigInteger("42540488241204005274814694018844196863");
        LAST_32BITS = new BigInteger("4294967295");
        COUNTRY_POSITION = new int[] { 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
        REGION_POSITION = new int[] { 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };
        CITY_POSITION = new int[] { 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };
        ISP_POSITION = new int[] { 0, 0, 3, 0, 5, 0, 7, 5, 7, 0, 8, 0, 9, 0, 9, 0, 9, 0, 9, 7, 9, 0, 9, 7, 9 };
        LATITUDE_POSITION = new int[] { 0, 0, 0, 0, 0, 5, 5, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
        LONGITUDE_POSITION = new int[] { 0, 0, 0, 0, 0, 6, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 };
        DOMAIN_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 6, 8, 0, 9, 0, 10, 0, 10, 0, 10, 0, 10, 8, 10, 0, 10, 8, 10 };
        ZIPCODE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 7, 0, 7, 7, 7, 0, 7, 0, 7, 7, 7, 0, 7 };
        TIMEZONE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 7, 8, 8, 8, 7, 8, 0, 8, 8, 8, 0, 8 };
        NETSPEED_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 11, 0, 11, 8, 11, 0, 11, 0, 11, 0, 11 };
        IDDCODE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 12, 0, 12, 0, 12, 9, 12, 0, 12 };
        AREACODE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 13, 0, 13, 0, 13, 10, 13, 0, 13 };
        WEATHERSTATIONCODE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 14, 0, 14, 0, 14, 0, 14 };
        WEATHERSTATIONNAME_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 15, 0, 15, 0, 15, 0, 15 };
        MCC_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 16, 0, 16, 9, 16 };
        MNC_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 17, 0, 17, 10, 17 };
        MOBILEBRAND_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 18, 0, 18, 11, 18 };
        ELEVATION_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 19, 0, 19 };
        USAGETYPE_POSITION = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 20 };
    }
}