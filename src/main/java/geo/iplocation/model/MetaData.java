// 
// Decompiled by Procyon v0.5.36
// 

package geo.iplocation.model;

class MetaData {
    private int _BaseAddr;
    private int _DBCount;
    private int _DBColumn;
    private int _DBType;
    private int _DBDay;
    private int _DBMonth;
    private int _DBYear;
    private int _BaseAddrIPv6;
    private int _DBCountIPv6;
    private boolean _OldBIN;
    private boolean _Indexed;
    private boolean _IndexedIPv6;
    private int _IndexBaseAddr;
    private int _IndexBaseAddrIPv6;

    MetaData() {
        this._BaseAddr = 0;
        this._DBCount = 0;
        this._DBColumn = 0;
        this._DBType = 0;
        this._DBDay = 1;
        this._DBMonth = 1;
        this._DBYear = 1;
        this._BaseAddrIPv6 = 0;
        this._DBCountIPv6 = 0;
        this._OldBIN = false;
        this._Indexed = false;
        this._IndexedIPv6 = false;
        this._IndexBaseAddr = 0;
        this._IndexBaseAddrIPv6 = 0;
    }

    int getBaseAddr() {
        return this._BaseAddr;
    }

    int getDBCount() {
        return this._DBCount;
    }

    int getDBColumn() {
        return this._DBColumn;
    }

    int getDBType() {
        return this._DBType;
    }

    int getDBDay() {
        return this._DBDay;
    }

    int getDBMonth() {
        return this._DBMonth;
    }

    int getDBYear() {
        return this._DBYear;
    }

    int getBaseAddrIPv6() {
        return this._BaseAddrIPv6;
    }

    int getDBCountIPv6() {
        return this._DBCountIPv6;
    }

    boolean getOldBIN() {
        return this._OldBIN;
    }

    boolean getIndexed() {
        return this._Indexed;
    }

    boolean getIndexedIPv6() {
        return this._IndexedIPv6;
    }

    int getIndexBaseAddr() {
        return this._IndexBaseAddr;
    }

    int getIndexBaseAddrIPv6() {
        return this._IndexBaseAddrIPv6;
    }

    void setBaseAddr(final int baseAddr) {
        this._BaseAddr = baseAddr;
    }

    void setDBCount(final int dbCount) {
        this._DBCount = dbCount;
    }

    void setDBColumn(final int dbColumn) {
        this._DBColumn = dbColumn;
    }

    void setDBType(final int dbType) {
        this._DBType = dbType;
    }

    void setDBDay(final int dbDay) {
        this._DBDay = dbDay;
    }

    void setDBMonth(final int dbMonth) {
        this._DBMonth = dbMonth;
    }

    void setDBYear(final int dbYear) {
        this._DBYear = dbYear;
    }

    void setBaseAddrIPv6(final int baseAddrIPv6) {
        this._BaseAddrIPv6 = baseAddrIPv6;
    }

    void setDBCountIPv6(final int dbCountIPv6) {
        this._DBCountIPv6 = dbCountIPv6;
    }

    void setOldBIN(final boolean oldBIN) {
        this._OldBIN = oldBIN;
    }

    void setIndexed(final boolean indexed) {
        this._Indexed = indexed;
    }

    void setIndexedIPv6(final boolean indexedIPv6) {
        this._IndexedIPv6 = indexedIPv6;
    }

    void setIndexBaseAddr(final int indexBaseAddr) {
        this._IndexBaseAddr = indexBaseAddr;
    }

    void setIndexBaseAddrIPv6(final int indexBaseAddrIPv6) {
        this._IndexBaseAddrIPv6 = indexBaseAddrIPv6;
    }
}