package geo.iplocation.model;

/**
 * Hello world!
 *
 */
public class App {

    private static final String CONFIG_PATH_IPV6 = "configs/" + "IPV6-COUNTRY-REGION-CITY.BIN";

    private static final String CONFIG_PATH_IPV4 = "configs/" + "IP-COUNTRY-REGION-CITY.BIN";

    private static final IP2Location locIpV6 = new IP2Location();
    private static final IP2Location locIpV4 = new IP2Location();

    static {
        locIpV6.IPDatabasePath = CONFIG_PATH_IPV6;
        locIpV6.IPLicensePath = "";

        locIpV4.IPDatabasePath = CONFIG_PATH_IPV4;
    }

    public static String getProvince(String ipAddress) {
        return "";

    }

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        for (int i = 0; i<=10000 ; i++) {
            try {

                IPResult rec = locIpV6.IPQuery("2402:800:61c5:baf2:150a:e52c:18f0:50db");
                System.out.println(rec.getRegion().toString());

            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace(System.out);
            }
        }
        System.out.println(System.currentTimeMillis() - startTime);
    }

}
