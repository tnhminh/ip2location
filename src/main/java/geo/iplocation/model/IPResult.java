package geo.iplocation.model;


public class IPResult
{
    static final String NOT_SUPPORTED = "Not_Supported";
    String ip_address;
    String country_short;
    String country_long;
    String region;
    String city;
    String isp;
    float latitude;
    float longitude;
    String domain;
    String zipcode;
    String netspeed;
    String timezone;
    String iddcode;
    String areacode;
    String weatherstationcode;
    String weatherstationname;
    String mcc;
    String mnc;
    String mobilebrand;
    float elevation;
    String usagetype;
    String status;
    boolean delay;
    String version;
    
    IPResult(final String ip_address) {
        this.delay = false;
        this.version = "Version 8.2.0";
        this.ip_address = ip_address;
    }
    
    public String getCountryShort() {
        return this.country_short;
    }
    
    public String getCountryLong() {
        return this.country_long;
    }
    
    public String getRegion() {
        return this.region;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public String getISP() {
        return this.isp;
    }
    
    public float getLatitude() {
        return this.latitude;
    }
    
    public float getLongitude() {
        return this.longitude;
    }
    
    public String getDomain() {
        return this.domain;
    }
    
    public String getZipCode() {
        return this.zipcode;
    }
    
    public String getTimeZone() {
        return this.timezone;
    }
    
    public String getNetSpeed() {
        return this.netspeed;
    }
    
    public String getIDDCode() {
        return this.iddcode;
    }
    
    public String getAreaCode() {
        return this.areacode;
    }
    
    public String getWeatherStationCode() {
        return this.weatherstationcode;
    }
    
    public String getWeatherStationName() {
        return this.weatherstationname;
    }
    
    public String getMCC() {
        return this.mcc;
    }
    
    public String getMNC() {
        return this.mnc;
    }
    
    public String getMobileBrand() {
        return this.mobilebrand;
    }
    
    public float getElevation() {
        return this.elevation;
    }
    
    public String getUsageType() {
        return this.usagetype;
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public boolean getDelay() {
        return this.delay;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    @Override
    public String toString() {
        final String property = System.getProperty("line.separator");
        final StringBuffer sb = new StringBuffer("IP2LocationRecord:" + property);
        sb.append("\tIP Address = " + this.ip_address + property);
        sb.append("\tCountry Short = " + this.country_short + property);
        sb.append("\tCountry Long = " + this.country_long + property);
        sb.append("\tRegion = " + this.region + property);
        sb.append("\tCity = " + this.city + property);
        sb.append("\tISP = " + this.isp + property);
        sb.append("\tLatitude = " + this.latitude + property);
        sb.append("\tLongitude = " + this.longitude + property);
        sb.append("\tDomain = " + this.domain + property);
        sb.append("\tZipCode = " + this.zipcode + property);
        sb.append("\tTimeZone = " + this.timezone + property);
        sb.append("\tNetSpeed = " + this.netspeed + property);
        sb.append("\tIDDCode = " + this.iddcode + property);
        sb.append("\tAreaCode = " + this.areacode + property);
        sb.append("\tWeatherStationCode = " + this.weatherstationcode + property);
        sb.append("\tWeatherStationName = " + this.weatherstationname + property);
        sb.append("\tMCC = " + this.mcc + property);
        sb.append("\tMNC = " + this.mnc + property);
        sb.append("\tMobileBrand = " + this.mobilebrand + property);
        sb.append("\tElevation = " + this.elevation + property);
        sb.append("\tUsageType = " + this.usagetype + property);
        return sb.toString();
    }
}